var fs = require('fs'),
  child_process = require('child_process'),
  path = require('path'),
  rimraf = require('rimraf'),
  util = require('util'),
  request = require('request'),
  async = require('async'),
  zipfile = require('zipfile');

var minecraft_mgr = function(options) {
  this.options = options;
  this.options.path = path.join('instances', options.instance);
  createSubDirs(this.options.path);
};

minecraft_mgr.InstallType = {
  ZIP: {
    value: 1,
    setUrl: function(options, callback) {
      callback();
    },
    installFiles: function(options, callback) {
      HandleZip(options.url, options.path, callback);
    },
    postInstall: function(options, callback) {}
  },
  FTB_ZIP: {
    value: 2,
    setUrl: function(options, callback) {
      options.url = util.format('http://www.creeperrepo.net/FTB2/modpacks%5E%s%5E%s%5E%sServer.zip',
        options.pack, options.version.replace(/\./g, '_'), options.pack);
      console.log(options.url);
      callback();
    },
    installFiles: function(options, callback) {
      HandleZip(options.url, options.path, callback);
    },
    postInstall: function(options, callback) {
      var command = 'bash FTBInstall.sh';
      console.log('Executing post install: ' + command);
      child_process.execSync(command, {
        cwd: options.path
      });
      callback();
    }
  },
  ATL_ZIP: {
    value: 3,
    setUrl: function(options, callback) {
      getAtlUrl(options, callback);
    },
    installFiles: function(options, callback) {
      HandleZip(options.url, options.path, callback);
    },
    postInstall: function(options, callback) {
      var command = 'java -jar SetupServer.jar';
      console.log('Executing post install: ' + command);
      child_process.execSync(command, {
        cwd: options.path
      });
      callback();
    }
  }
};

function HandleZip(url, install_path, callback) {
  var tmpFile = '/tmp/download.zip';
  request
    .get(url)
    .on('response', function(response) {
      console.time('Zip Download');
      console.log(response.statusCode); // 200
      console.log(response.headers['content-type'])
      this.len = parseInt(response.headers['content-length'], 10);
      this.cur = 0;
    })
    .on('data', function(chunk) {
      this.cur += chunk.length;
      process.stdout.write("Downloading " + (100.0 * this.cur / this.len).toFixed(2) + "% " + this.cur + " bytes\033[0G");
    })
    .on('end', function() {
      console.log();
      console.timeEnd("Zip Download");
      console.log("Download Complete");
    })
    .pipe(fs.createWriteStream(tmpFile).on('close', function() {
      var zf = new zipfile.ZipFile(tmpFile);
      zf.names.forEach(function(name) {
        if (name.indexOf('/', name.length - 1) === -1) {
          console.log('Writing: ' + name);
          var dest = path.join(install_path, name);
          createSubDirs(install_path, name);
          zf.copyFileSync(name, dest);
        }
      });
      callback();
    }));
}

function createSubDirs(install_path, dest_path) {
  createDirsRecursive(path.join(install_path, path.dirname(dest_path)));
}

function createDirsRecursive(dir) {
  var parent = path.join(dir, '..');

  // Base case: Directory exists
  if (fs.existsSync(dir)) {
    return;
  }

  createDirsRecursive(parent);
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
}

function getAtlUrl(options, callback) {
  var pack = options.pack;
  var version = options.version;
  var packUrl = util.format('https://api.atlauncher.com/v1/pack/%s/%s', pack, version);

  request(packUrl, function(error, response, body) {
    if (!error && response.statusCode == 200) {
      var obj = JSON.parse(body);
      options.url = obj.data.serverZipURL;
      console.log(options.url);
      callback();
    }
  });
}

minecraft_mgr.prototype.createInstance = function() {
  if (!fs.existsSync(this.options.path)) {
    fs.mkdirSync(this.options.path);
  }
  return this;
};

minecraft_mgr.prototype.removeInstance = function() {
  if (fs.existsSync(this.options.path)) {
    rimraf.sync(this.options.path);
  }
  return this;
};

minecraft_mgr.prototype.touchEula = function() {
  var eulaPath = path.join(this.options.path, 'eula.txt');
  fs.writeFileSync(eulaPath, 'eula=true');
  return this;
}

minecraft_mgr.prototype.install = function() {

  var options = this.options;

  if (options.install_type === undefined) {
    throw Error('Missing install type');
  }

  // Execute each step in-order
  async.series([

    function(callback) {
      options.install_type.setUrl(options, callback);
    },
    function(callback) {
      options.install_type.installFiles(options, callback);
    },
    function(callback) {
      options.install_type.postInstall(options, callback);
    },
    this.writeInstanceFile
  ]);

  return this;
};

minecraft_mgr.prototype.writeInstanceFile = function(callback) {
  var instanceFileContents = JSON.stringify({
    name: name,
    version: version,
    serverExec: serverExec
  }, null, 4);
  fs.writeFileSync(path.join(install_dir, 'instance.json'), instanceFileContents);
  callback();
}

module.exports = minecraft_mgr;
