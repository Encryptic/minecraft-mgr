#!/usr/bin/env node

/**
 * Module dependencies.
 */
var minecraft_mgr = require('./lib/minecraft-mgr'),
  program = require('commander'),
  path = require('path'),
  basename = path.basename,
  dirname = path.dirname;

program
  .version(require('./package.json').version)
  .option('--verbose', 'Enable verbose mode');

program
  .command('remove <instance>')
  .action(function(instance) {
    // handle the response
    var manager = new minecraft_mgr({
      instance: instance
    });
    manager.removeInstance();
  });

program
  .command('install-zip <instance> <url>')
  .action(function(instance, url) {
    var manager = new minecraft_mgr({
      instance: instance,
      url: url,
      install_type: minecraft_mgr.InstallType.ZIP
    });
    manager.createInstance().install();
  });

program
  .command('install-atl <instance> <pack> <version>')
  .action(function(instance, pack, version) {
    var manager = new minecraft_mgr({
      instance: instance,
      pack: pack,
      version: version,
      install_type: minecraft_mgr.InstallType.ATL_ZIP
    });
    manager.createInstance().install();
  });

program
  .command('install-ftb <instance> <pack> <version>')
  .action(function(instance, pack, version) {
    var manager = new minecraft_mgr({
      instance: instance,
      pack: pack,
      version: version,
      install_type: minecraft_mgr.InstallType.FTB_ZIP
    });
    manager.createInstance().install();
  });

program.command('touch-eula <instance>')
  .action(function(instance) {
    console.log('By changing the setting to TRUE you are indicating your agreement to our EULA (https://account.mojang.com/documents/minecraft_eula)');
    var manager = new minecraft_mgr({
      instance: instance
    });
    manager.touchEula();
  });

program.on('--help', function() {
  console.log('  Examples:');
  console.log('  install-ftb InstanceName FTBInfinity 1.0.2');

  console.log('');
});

program.on('--verbose', function() {
  console.log(program);
});

program.parse(process.argv);
